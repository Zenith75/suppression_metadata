# Suppression_Metadata

# Application de Gestion des Métadonnées d'Images

## Description
Cette application permet aux utilisateurs de charger des images, d'afficher et d'effacer leurs métadonnées, et de télécharger les images modifiées. Elle est construite avec Python et utilise Tkinter pour l'interface graphique.

## Installation

### Prérequis
- Python 3.x
- Tkinter (généralement inclus dans l'installation standard de Python)

### Installation des dépendances
Exécutez la commande suivante pour installer les dépendances nécessaires :
pip install -r requirements.txt


## Utilisation

Pour lancer l'application, exécutez le script Python depuis votre terminal :
python clearmetadata.py



### Fonctionnalités
- **Upload Image** : Permet de charger une image pour afficher ses métadonnées.
- **Effacer Métadonnées** : Supprime les métadonnées de l'image chargée.
- **Télécharger Image** : Enregistre l'image modifiée sur votre système.



## Exemple d'utilisation
1. Cliquez sur `Upload Image` pour charger une image.
2. Visualisez les métadonnées dans la zone de texte.
3. Cliquez sur `Effacer Métadonnées` pour supprimer les métadonnées de l'image.
4. Enregistrez l'image modifiée en cliquant sur `Télécharger Image`

