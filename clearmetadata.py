import tkinter as tk
from tkinter import filedialog, LabelFrame
from PIL import Image
import piexif

def upload_image():
    global image_path
    image_path = filedialog.askopenfilename()
    if image_path:
        metadata_text.delete('1.0', tk.END)
        metadata_text.insert(tk.END, get_metadata(image_path))

def get_metadata(image_path):
    metadata = piexif.load(image_path)
    return str(metadata)

def clear_metadata():
    if image_path:
        piexif.remove(image_path)
        metadata_text.delete('1.0', tk.END)
        metadata_text.insert(tk.END, "Métadonnées effacées.")

def download_image():
    if image_path:
        image = Image.open(image_path)
        save_path = filedialog.asksaveasfilename(defaultextension=".jpg")
        image.save(save_path)

app = tk.Tk()
app.title("Application de Gestion des Métadonnées d'Images")
app.config(bg="black")

# Disposition des boutons et de la zone de texte
frame_buttons = tk.Frame(app, bg="black")
frame_buttons.pack(pady=10)

upload_btn = tk.Button(frame_buttons, text="Upload Image", command=upload_image, bg="green", fg="white")
upload_btn.pack(side=tk.LEFT, padx=5)

clear_metadata_btn = tk.Button(frame_buttons, text="Effacer Métadonnées", command=clear_metadata, bg="green", fg="white")
clear_metadata_btn.pack(side=tk.LEFT, padx=5)

download_btn = tk.Button(frame_buttons, text="Télécharger Image", command=download_image, bg="green", fg="white")
download_btn.pack(side=tk.LEFT, padx=5)

# Zone d'affichage des métadonnées
frame_metadata = LabelFrame(app, text="Métadonnées", bg="black", fg="green")
frame_metadata.pack(padx=10, pady=10, fill="both", expand="yes")

metadata_text = tk.Text(frame_metadata, bg="black", fg="green")
metadata_text.pack(padx=5, pady=5, fill="both", expand="yes")

app.mainloop()